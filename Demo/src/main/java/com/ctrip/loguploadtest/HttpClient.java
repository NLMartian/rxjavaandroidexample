package com.ctrip.loguploadtest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/**
 * Created by jgzhu on 14-2-14.
 */
public class HttpClient {

    private static HttpClient instatnce;

    private HttpClient() {
    }

    public static HttpClient getInstance() {
        if (instatnce == null) {
            synchronized (HttpClient.class) {
                if (instatnce == null) {
                    instatnce = new HttpClient();
                }
            }
        }
        return instatnce;
    }

    public String performGet() {
        HttpURLConnection connection = null;
        try {
            URL url = new URL("http://www.baidu.com");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            //connection.setDoOutput(true);
            connection.connect();

            BufferedReader bufferedReader;
            InputStream is;

            if (connection.getResponseCode() == 200) {
                is = connection.getInputStream();
                String strConEncode = connection.getContentEncoding();
                if (strConEncode != null && !strConEncode.equals("")
                        && strConEncode.equals("gzip")) {
                    is = new GZIPInputStream(is);
                }
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sbResult = new StringBuilder();
                String strLine;

                while ((strLine = bufferedReader.readLine()) != null) {
                    sbResult.append(strLine);
                }

                return sbResult.toString();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
