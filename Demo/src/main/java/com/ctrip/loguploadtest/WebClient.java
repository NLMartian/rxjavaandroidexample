package com.ctrip.loguploadtest;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by jgzhu on 14-2-14.
 */
public class WebClient {

    public static Observable<String> getData() {

        return Observable.create(new Observable.OnSubscribeFunc<String>() {

            @Override
            public Subscription onSubscribe(Observer<? super String> observer) {

                try {
                    observer.onNext(HttpClient.getInstance().performGet());
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }

                return Subscriptions.empty();
            }

        });
    };

    public static Observable<Integer> loopObservable() {
        return Observable.create(new Observable.OnSubscribeFunc<Integer>() {

            @Override
            public Subscription onSubscribe(Observer<? super Integer> observer) {
                try {
                    int i = execLoop();
                    observer.onNext(i);
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }

                return Subscriptions.empty();
            }
        });
    }

    private static Integer execLoop() throws InterruptedException {
        int i = 0;
        while (i != Integer.MAX_VALUE) {
            i--;
            i++;
        }

        return i;
    }

}
