package com.ctrip.loguploadtest;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import rx.Observer;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.schedulers.Schedulers;

public class MainActivity extends ActionBarActivity{

    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DummyFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        Subscription subscription = null;
        switch (item.getItemId()) {
            case R.id.action_settings:
                pd = new ProgressDialog(this);
                pd.setMessage("请稍候");
                pd.show();
                subscription = AndroidObservable.bindActivity(this, WebClient.loopObservable())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(pageObserver);
                break;
            case R.id.action_stop:
                if (pd != null) {
                    pd.dismiss();
                }

                if (subscription != null) {
                    subscription.unsubscribe();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Observer<Integer> pageObserver = new Observer<Integer>() {
        @Override
        public void onCompleted() {
            Toast.makeText(MainActivity.this, "完成了", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(Throwable throwable) {
            pd.dismiss();
            Toast.makeText(MainActivity.this, "出错了。", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onNext(Integer i) {
            pd.dismiss();
            Toast.makeText(MainActivity.this, i.toString(), Toast.LENGTH_LONG).show();
        }
    };

    /**
     * A dummy fragment containing a simple view.
     */
    public static class DummyFragment extends Fragment {

        public DummyFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
